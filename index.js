const express = require('express');
const http = require("http");
Request = require("request");
var uuid=require('uuid');
var fs = require('fs');

app = express(),
server = http.createServer(/*{
    key:fs.readFileSync('localhost.key'),
    cert:fs.readFileSync('localhost.crt')
},*/app),
userserver = http.createServer(/*{
    key:fs.readFileSync('localhost.key'),
    cert:fs.readFileSync('localhost.crt')
},*/app);

io = require('socket.io').listen(server);
ioUser = require('socket.io').listen(userserver);


var management_users=[];
management_users.push({"username":"AdminPanel","password":"pass123"});
management_users.push({"username":"CommunicationServer","password":"pass123"});
const linkedList = require('./linked-list');
const users = linkedList();
const management = linkedList();
const audio = linkedList();
const onCallUsers = linkedList();


app.get('/', (req, res) => {

res.send('Chat Server is running on port 3000')
});


server.listen(3000,()=>{

console.log('Management Server is running on port 3000')

});
userserver.listen(3001,()=>{
console.log("User server is running on port 3001")
});

/*sServer.listen(3002,()=>{
console.log("secure server activated")
});*/

io.on('connection', (socket) => {
    var username;
     socket.on('join_management', function(data) {
         var dt = JSON.parse(data);
         var login=false;
         for(i=0;i<management_users.length;i++){
             var some_data=management_users[i];
             if(some_data.username==dt.username){
                 if(some_data.password==dt.password){
                    login=true;
                 }
             }
         }

         var response=[];
         if(login)
         {
          var user = management.find(dt.username);
          if(user!=null){
            management.add({"username":dt.username,"socket":socket});
          }else{
            management.replace(username,{"username":dt.username,"socket":socket});
          }
            username=dt.username;
            if(username=="CommunicationServer"){
               sendCommunicationStatusToAdmin();
           }
             }
        response.push({"Status":login,"Command":"Login"});
        socket.emit("management",JSON.stringify(response));
        });

        socket.on('disconnect',function(data){
            management.remove(username);
                if(username=="CommunicationServer"){
                    sendCommunicationStatusToAdmin();
                }
        });
        socket.on('management',function(data){
          var dt=JSON.parse(data);
          if(dt.hasOwnProperty('status')){
              if(dt.status=='command'){
                  if(dt.server_status=='CommunicationServer'){
                       sendCommunicationStatusToAdmin();
              }
          }else if(dt.status=='users'){
            sendUserInformationToServer();
        }
        }
        });
        socket.on("server-info",function(data){
            var jsonData = JSON.parse(data);
            var call = audio.find(jsonData.SessionId);
            call["port"]=jsonData.PORT;
            call["IPAddress"]=jsonData.IPAddress;
            audio.replace(jsonData.SessionId,call);
            var user = users.find(call.caller);
            var receiver = users.find(call.receiver);
            user.socket.emit("call-status",{"status":"Found","PORT":call.port,"SessionId":call.username,"IPAddress":call.IPAddress,"SessionId":call.username});
            receiver.socket.emit("SomeoneCalling",{"PORT":call.port,"SessionId":call.username,"IPAddress":call.IPAddress,"Caller":call.caller,"Type":"personal"});
            onCallUsers.add({"username":call.caller});
            onCallUsers.add({"username":call.receiver});
            var logData = {};
            logData["date"]=jsonData.date;
            logData["caller"]=call.caller;
            logData["receiver"]=call.receiver;
            logData["port"]=jsonData.PORT;
            logData["ipAddress"]=jsonData.IPAddress;
            Request.post({
                "headers": { "content-type": "application/json" },
                "url": "http://192.168.100.56:8080/AdminPanel/api/history/insertPersonalCallHistory",
                "body": JSON.stringify({
                    logData
                })            }, (error, response, body) => {
                if(error) {
                    return console.dir(error);
                }
                console.dir(JSON.parse(body));
            });
        });

        socket.on("server-info-department",function(data){
            var jsonData = JSON.parse(data);
            var call = audio.find(jsonData.SessionId);
            call["port"]=jsonData.PORT;
            call["IPAddress"]=jsonData.IPAddress;
            audio.replace(jsonData.SessionId,call);
            var user = users.find(call.caller);
            console.log("broadcast call");
            user.socket.broadcast.emit(call.Department+"-call",{"PORT":call.port,"SessionId":call.username,
            "IPAddress":call.IPAddress,"Caller":call.caller,"Type":"group"});
        });
    });

    function sendCommunicationStatusToAdmin(){
        var response=[];
        var status=false;
        var user = management.find("CommunicationServer");
            if(user!=null){
                status=true;
        }
        var user1 = management.find("AdminPanel");
        if(user1!=null){
            response.push({"Command":"ServerStatus","ServerName":"CommunicationServer",
                            "ServerStatus":status});
            user1.socket.emit("management",JSON.stringify(response));
        }
    }

    function sendUserInformationToServer(){
        var response=[];
        var user=management.find("AdminPanel");
            if(user!=null){
                var datas = users.toArray();
                //console.log(datas);
                for(j=0;j<datas.length;j++){
                    var data=datas[j];
                    response.push({"Command":"UserConnection","Status":"Connected",
                                    "Username":data.Username,"UserId":data.UserId,
                                    "Fullname":data.Fullname});
                    user.socket.emit("management",JSON.stringify(response));
                }
                }
    }




    //User Code start From Here
    ioUser.on("connection",function(socket){
        var username;
        let handshake = socket.handshake;
        socket.on("join_user",function(rData){
            var data = JSON.parse(rData);
            username=data.Username;
            if(true)
            {
                var ip = handshake.address;
                var result = ip.split(":");
                ip = result[result.length-1];
                var response=[];
                var user = users.find(username);
                if(user==null){
                    users.add({"username":data.Username,"socket":socket,"UserId":data.UserId,
                                "Fullname":data.Fullname,"IPAddress":ip});
                }else{
                    users.replace(username,{"username":data.Username,"socket":socket,"UserId":data.UserId,
                                            "Fullname":data.Fullname,"IPAddress":ip})
                }

                var manage = management.find("AdminPanel");
                if(manage!=null){
                    response.push({"Command":"UserConnection","Status":"Connected",
                                   "Username":data.Username,
                                    "UserId":data.UserId,"Fullname":data.Fullname});
                    manage.socket.emit("management",JSON.stringify(response));
                }
                }
                socket.on("disconnect",function(rData){
                    var user=users.find(username);
                    users.remove(username);
                    var manage = management.find("AdminPanel");
                    if(manage!=null){
                        response.push({"Command":"UserConnection","Status":"Disconnected",
                                       "UserId":user.UserId});
                        manage.socket.emit("management",JSON.stringify(response));
                    }
                    var usr = audio.find(username);
                    if(usr!=null){
                        audio.remove(username);
                    }
                });

                //handel messaging
                socket.on("message",function(rData){
                    Request.post({
                        "headers": { "content-type": "application/json" },
                        "url": "http://192.168.100.56:8080/AdminPanel/api/department-message/insert",
                        "body": JSON.stringify({
                            rData
                        })
                    }, (error, response, body) => {
                        if(error) {
                            return console.dir(error);
                        }
                    });
                    socket.broadcast.emit(rData.Department,rData);
                });

                socket.on("pMessage",function(rData){
                    var user=users.find(rData.Receiver);
                    console.log(rData);
                    if(user!=null){
                        rData['deliver']=true;
                        user.socket.emit(rData.Receiver,rData);
                        console.log(user.username+" send message to "+user.receiver);
                     }else{
                         rData['deliver']=false;
                     }
                Request.post({
                    "headers": { "content-type": "application/json" },
                    "url": "http://192.168.100.56:8080/AdminPanel/api/department-message/personal-message",
                    "body": JSON.stringify({
                        rData
                    })
                }, (error, response, body) => {
                    if(error) {
                        return console.dir(error);
                    }
                    console.dir(JSON.parse(body));
                });

                });

                socket.on("initialize-call",function(data){
                    data=JSON.parse(data);
                    var user = users.find(data.receiver);
                    var sender = users.find(data.sender);
                   // console.log(user);
                    if(user!=null){
                    var checkonCall = onCallUsers.find(user.username);
                    if(checkonCall==null){
                    var callId=uuid.v4();
                    var manage = management.find("CommunicationServer");
                    audio.add({"username":callId,"caller":sender.username,"receiver":user.username,"type":"personal"});
                    manage.socket.emit("create-server",{"SessionId":callId,"type":"personal",
                                        data:[{"user":user.username,"address":user.IPAddress},
                                              {"user":sender.username,"address":sender.IPAddress}]});
                        }else{
                            socket.emit("call-status",{"status":"UserBusy"});
                        }
                    }else{
                       socket.emit("call-status",{"status":"NotFound"});
                    }
                });

                socket.on("department-call",function(data){
                    console.log(data);
                    var user = users.find(data.caller);
                    var department = data.department;
                    console.log(data.caller);
                    if(user!=null){
                        var callId = uuid.v4();
                        var manage = management.find("CommunicationServer");
                        audio.add({"username":callId,"Department":department,"caller":user.username,"type":"group"});
                        console.log("department call initialize");
                        manage.socket.emit("create-server",{"SessionId":callId,"type":"group",
                                            data:[{"user":user.username,"address":user.IPAddress}]});
                    }
                });

                socket.on("add-to-department",function(data){
                    var manage = management.find("CommunicationServer");
                    var user = users.find(data.username);
                    console.log(users.toArray());
                    manage.socket.emit("add-user",{"username":user.username,"address":user.IPAddress,
                                        "SessionId":data.sessionId});
                    console.log("add user");
                });

                socket.on("remove-from-department",function(data){
                    var manage = management.find("CommunicationServer");
                    console.log("remove user");
                    manage.socket.emit("remove-user",{"username":data.username,"SessionId":data.sessionId});
                });

                socket.on("call-action",function(data){
                    var call = audio.find(data.SessionId);
                    var manage = management.find("CommunicationServer");
                    var admin = management.find("AdminPanel");
                    var response = [];
                    if(data.Action=="accept"){
                        if(data.Type=="personal"){
                            call["Status"] = true;
                            manage.socket.emit("call-ended",{"SessionId":data.SessionId});
                            if(admin!=null){
                                response.push({"SessionId":call.username,"Caller":call.caller,
                                "Receiver":call.receiver,"Command":"LiveCall","Status":"Connected"});
                                admin.socket.emit("management",JSON.stringify(response));
                            }
                        }else if(data.Type=="group"){
                            var user = users.find(data.Username);
                            manage.socket.emit("add-user",{"SessionId":data.SessionId,
                                                    "username":user.username,"address":user.IPAddress});
                        }
                    }else if(data.Action=="reject"){
                        if(data.Type=="personal"){
                            call["Status"] = false;
                            onCallUsers.remove(call.caller);
                            onCallUsers.remove(call.receiver);
                            manage.socket.emit("call-ended",{"SessionId":data.SessionId});
                        }else if(data.Type=="group"){
                            manage.socket.emit("remove-user",{"SessionId":data.SessionId,
                                               "username":data.username});
                        }
                    }
                    if(data.Type=="personal"){
                    audio.replace(data.SessionId,call);
                    console.log(call);
                    var user = users.find(call.caller);
                    var user1 = users.find(call.receiver);
                    user.socket.emit("call-action",data);
                    user1.socket.emit("call-action",data);
                    console.log("Data send");
                    }
                });



        });
    });
