//node for Single link list
const node = (val,pre,nex)=>{
    let value = val;
    let next = nex;
//set the value for node
const setValue=(v) => {
    value = v;
};
//set the value of the node
const getValue= (v) => value;

//set the value of the nex node
const setNext = (n) => {
    next = n;
};
//get the next node
const getNext = () => next||null;
return {
    setValue,
    getValue,
    setNext,
    getNext
};
};

// Single Link List
const linkedList = () => {
    let headNode = null;
    let nodesCount = 0;

    const head = () => headNode;

    const count = () => nodesCount;

    //Find the value from the list
    const find = (username) => {
        let current = headNode;
        while (current !== null){
            var data=current.getValue();
            if(data.username===username){
                return current.getValue();
            }
            current = current.getNext();
        }
        return current;
    };
    
    //add value to last of the list
    const add = (value) => {
        if (headNode === null) {
            headNode = node(value);
          } else {
            let current = headNode;
            while (current.getNext() !== null) {
              current = current.getNext();
            }
            current.setNext(node(value));
          }
          nodesCount += 1;
    };

    const removeFirst = () => {
        if (headNode !== null) {
          if (headNode.getNext() === null) {
            headNode = null;
          } else {
            headNode = headNode.getNext();
          }
          nodesCount -= 1;
        }
      };

    const remove = (value)=>{
        let prev = null;
        let current = headNode;
        while(current!==null){
            var data = current.getValue();
            if(data.username==value){
                if(prev===null){
                    removeFirst();
                }else{
                    prev.setNext(current.getNext());
                    nodesCount -= 1;}
                break;
            }else{
                prev = current;
                current = current.getNext();
            }}};

    const replace = (value,data) => {
      remove(value);
      add(data);
    };

    const traverse = (cb) => {
        let current = headNode;
        while (current !== null) {
          cb(current);
          current = current.getNext();
        }
      };

    const toArray = () => {
        const arr = [];
        traverse(n => arr.push(n.getValue()));
        return arr;
      };

    return {
        node,
        head,
        count,
        find,
        add,
        removeFirst,
        remove,
        traverse,
        replace,
        toArray
    };
    };

    module.exports = linkedList;

